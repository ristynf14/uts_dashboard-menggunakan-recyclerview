package com.ristynurfaadhilah_10191074.uts_membuatdashboardmenggunakanrecyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    RecyclerView rv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] text = new String[]{
                "My Book", "Calendar", "Profile",
                "Notes","My Number", "Notification",
                "Facebook", "Twitter", "Youtube"
        };

        int[] image = new int[] {
                R.drawable.book,
                R.drawable.calendar,
                R.drawable.ic_baseline_person_24,
                R.drawable.notepad,
                R.drawable.ic_baseline_call_24,
                R.drawable.ic_baseline_notifications_active_24,
                R.drawable.facebook,
                R.drawable.twitter,
                R.drawable.youtube,
        };

        rv = findViewById(R.id.recyclerview);
        RvAdapter adapter = new RvAdapter(this, text, image);
        rv.setAdapter(adapter);
        rv.setLayoutManager(new GridLayoutManager(this, 3));
    }
}